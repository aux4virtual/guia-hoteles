$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 5000
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log("El mensaje se esta mostrando");

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log("El mensaje se mostro");
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log("El mensaje se oculta");
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log("El mensaje se oculto");

        $('#contactoBtn').prop('disabled', false);
    });
});